//
//  TODOListCell.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import UIKit

class TODOListCell: UITableViewCell {

    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
