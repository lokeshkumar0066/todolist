//
//  ViewController.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import UIKit

class TODOListClassVC: UIViewController {

    
    @IBOutlet var viewSearchHeader: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    
   fileprivate var todoArrayList:[[String : Any]] = []
   private var tempTodoArrayList:[[String : Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.estimatedRowHeight = 100
        tblView.rowHeight = UITableView.automaticDimension
        viewSearch.cornerRadiusWithBorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadDataFromPersistance()
    }
    
    //MARK: Fetch Data from Local Storage
    private func loadDataFromPersistance(){
        todoArrayList = []
        tempTodoArrayList = []
        guard let data =  UserDefaults .standard .object(forKey: "todoList") else{
            self.lblNoDataFound.isHidden = false
            self.tblView.isHidden = true
            self.tblView.reloadData()
            return
        }
        todoArrayList = data as? [[String:Any]] ?? []
        tempTodoArrayList = todoArrayList
        self.lblNoDataFound.isHidden = true
        self.tblView.isHidden = false
        self.tblView.reloadData()
    }
    
    
    
    @IBAction func onClickAddBar(_ sender: Any) {
        txtSearch.resignFirstResponder()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vcObj: AddListClassVC = storyBoard.instantiateViewController(withIdentifier: "AddListClassVC") as! AddListClassVC
        vcObj.delegate = self
        let navController = UINavigationController(rootViewController: vcObj)
        self.present(navController, animated:true, completion: nil)
    }
    
    @IBAction func onEditingChangeSearch(_ sender: Any) {
        self.lblNoDataFound.isHidden = true
        self.tblView.isHidden = false
        let predicate = NSPredicate(format: "title contains[c] %@ OR description contains[c] %@", txtSearch.text!, txtSearch.text!)
        let searchDataSource = tempTodoArrayList.filter { predicate.evaluate(with: $0) }
        if searchDataSource.count > 0{
            todoArrayList = searchDataSource
            tblView.reloadData()
        }else{
            if txtSearch.text! == ""{
                todoArrayList = []
                todoArrayList = tempTodoArrayList
                tblView.reloadData()
            }else{
                todoArrayList = []
                tblView.reloadData()
            }
        }
    }
    
    
    
    @objc func btnCheckBox (sender: UIButton){
        let checkBoxType = todoArrayList[sender.tag]["checkBox"] as? Int ?? 0
        if checkBoxType == 0{
            let dict:[String:Any] = [
                    "checkBox" : 1,
                    "title" : todoArrayList[sender.tag]["title"] as? String ?? "",
                    "description":todoArrayList[sender.tag]["description"] as? String ?? "",
                    "imgFilePath" : todoArrayList[sender.tag]["imgFilePath"] as? Data ?? Data()
                ]
            
            todoArrayList.remove(at: sender.tag)
            todoArrayList.insert(dict, at: todoArrayList.count)
            
            UserDefaults.standard .removeObject(forKey: "todoList")
            UserDefaults.standard.set(todoArrayList, forKey: "todoList")
            
            tempTodoArrayList = []
            tempTodoArrayList = todoArrayList
            self.lblNoDataFound.isHidden = true
            self.tblView.isHidden = false
            tblView.reloadData()
        }
    }
    
}

extension TODOListClassVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoArrayList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewSearchHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewSearchHeader.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TODOListCell", for: indexPath) as! TODOListCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.viewContent.dropShadow()
        
        cell.lblTitle.text = todoArrayList[indexPath.row]["title"] as? String ?? ""
        cell.lblDescription.text = todoArrayList[indexPath.row]["description"] as? String ?? ""
        
        if let imageData = todoArrayList[indexPath.row]["imgFilePath"] as? Data{
            let imageFromData = UIImage(data: imageData)
            cell.imgView?.image = imageFromData!
        }else{
            cell.imgView?.image = UIImage(named: "placeholder.jpg")
        }
        
        cell.imgView?.cornerRadius()
        cell.imgView?.contentMode = .scaleAspectFill
        
        let checkBoxType = todoArrayList[indexPath.row]["checkBox"] as? Int ?? 0
        if checkBoxType == 1{
            cell.btnCheckBox.isSelected = true
            cell.viewContent.backgroundColor = UIColor.lightGray
        }else{
            cell.btnCheckBox.isSelected = false
            cell.viewContent.backgroundColor = UIColor.white
        }
        
        cell.btnCheckBox.tag = indexPath.row
        cell.btnCheckBox.addTarget(self, action: #selector(btnCheckBox(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtSearch.resignFirstResponder()
    }
    
}

extension TODOListClassVC: AddListClassVCDelegate{
    func updateList(){
        loadDataFromPersistance()
    }
}

extension TODOListClassVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSearch.resignFirstResponder()
        return true
    }
}


//com.logic.healthTrack.TODOList
