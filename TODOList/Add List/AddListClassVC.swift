//
//  AddListClassVC.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

protocol AddListClassVCDelegate {
    func updateList()
}


class AddListClassVC: UIViewController {

    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewDescription: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    public var delegate : AddListClassVCDelegate?
    private var arrayAddDeals:[[String:Any]] = []
    private var imgFilePath: String?
    
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgView.cornerRadius()
        viewTitle.cornerRadiusWithBorder()
        viewDescription.cornerRadiusWithBorder()
        cancelBtn.dropCornerRadius()
        saveBtn.dropCornerRadius()
        txtDescription.text = "Description"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Add ToDo item"
    }
    

    @IBAction func onClickImage(_ sender: Any) {
        collapseKeyboard()
        let actionSheet = UIAlertController(title: nil, message: "Choose an option", preferredStyle: .actionSheet)
        
        
        let cameraButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.openImagePickerController(sourceType: "camera")
        })
        
        let  PhotoLibrary = UIAlertAction(title: "PhotoLibrary", style: .default, handler: { (action) -> Void in
            self.openImagePickerController(sourceType: "photoLibrary")
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        
        actionSheet.addAction(cameraButton)
        actionSheet.addAction(PhotoLibrary)
        actionSheet.addAction(cancelButton)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    //MARK: Open Image Picker

    func openImagePickerController(sourceType: String) {
        
        if sourceType == "camera" {
            if UIImagePickerController.availableCaptureModes(for: .front) != nil {
                imagePicker.delegate = self
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .camera
                present(imagePicker, animated: true, completion: nil)
            } else {
                self.showAlertWithCallback(message: cameraError, title: appTitle, buttonName: "OK") { (type) -> (Void) in
                    
                }
            }
            
        }else{
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
//    func writeImageFile(){
//        let directoryPath =  NSHomeDirectory().appending("/Documents/")
//        if !FileManager.default.fileExists(atPath: directoryPath) {
//            do {
//                try FileManager.default.createDirectory(at: NSURL.fileURL(withPath: directoryPath), withIntermediateDirectories: true, attributes: nil)
//            } catch {
//                print(error)
//            }
//        }
//        let filename = Date().string(format: "yyyyMMddhhmmss").appending(".jpg")
//        let filepath = directoryPath.appending(filename)
//        let url = NSURL.fileURL(withPath: filepath)
//        do {
//            try imgView.image!.jpegData(compressionQuality: 1.0)?.write(to: url, options: .atomic)
//            print(filepath)
//            imgFilePath = filepath
//            print("Image Saved")
//        } catch {
//            print(error)
//            print("file cant not be save at path \(filepath), with error : \(error)");
//
//        }
//
//    }
    
    
    
    
    @IBAction func onClickCancel(_ sender: Any) {
        collapseKeyboard()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        validation()
    }
    
    
    private func collapseKeyboard(){
        txtTitle.resignFirstResponder()
        txtDescription.resignFirstResponder()
    }
    
    private func validation(){
        collapseKeyboard()
        
        if txtTitle.text == "" || txtTitle.text?.count == 0{
            showAlertWithCallback(message: titleError, title: appTitle, buttonName: "OK") { (type) -> (Void) in
                
            }
        } else if txtDescription.text == "Description" || txtDescription.text == "" || txtDescription.text.count == 0 {
            showAlertWithCallback(message: descriptionError, title: appTitle, buttonName: "OK") { (type) -> (Void) in
                
            }
        } else {
            createLocalPersistance()
        }
    }
    
    
    
    //MARK: Add Data into Local Storage
      private func createLocalPersistance(){
           var localArray:[[String:Any]] = []
           
           if let data = UserDefaults.standard.object(forKey: "todoList") as? [[String:Any]], data.count > 0 {
               
            localArray =  addDataIntoLocalPersistance(checkBox: 0, title: txtTitle.text!, description: txtDescription.text!,imageData: imgView.image!.jpegData(compressionQuality: 100)!)

            
               for dict in data {
                   if dict.count > 0{
                       let checkBox = dict["checkBox"] as? Int ?? 0
                       let title = dict["title"] as? String ?? ""
                       let description = dict["description"] as? String ?? ""
                       let imageData = dict["imgFilePath"] as? Data ?? Data()
                    localArray = addDataIntoLocalPersistance(checkBox: checkBox, title: title, description: description, imageData:imageData)
                   }
               }
               
           }else{
               UserDefaults.standard .removeObject(forKey: "todoList")
            localArray =  addDataIntoLocalPersistance(checkBox: 0, title: txtTitle.text!, description: txtDescription.text!, imageData: imgView.image!.jpegData(compressionQuality: 100)!)
           }
           
           UserDefaults.standard .removeObject(forKey: "todoList")
           UserDefaults.standard.set(localArray, forKey: "todoList")
           self.dismiss(animated: true) {
               self.delegate?.updateList()
           }
       }
    
    
    private func addDataIntoLocalPersistance(checkBox: Int,title: String, description: String, imageData: Data) -> [[String:Any]]{
        let dict:[String:Any] = [
                "checkBox" : checkBox,
                "title" : title,
                "description":description,
                "imgFilePath" : imageData
            ]
        
        arrayAddDeals.append(dict)
        return arrayAddDeals
    }
    
    
}

extension AddListClassVC: UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool{
        if textView.text == "Description"{
            txtDescription.text = ""
        }
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool{
        if textView.text == "Description"{
            txtDescription.text = ""
        }
        return true
    }


    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        if text == "\n"{
            txtDescription.resignFirstResponder()
            self.validation()
            return false
        }
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        if newText == ""{
//
//        }
        return true
    }

    
}


extension AddListClassVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtTitle.resignFirstResponder()
        return true
    }
}

extension AddListClassVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
      func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

          if picker.sourceType == .camera {
              if let pickedImage = info[.editedImage] as? UIImage {
                  imgView.contentMode = .scaleAspectFill
                  imgView.image = pickedImage
              }else{
                  imgView.contentMode = .scaleAspectFill
                  imgView.image = info[.originalImage] as? UIImage
              }
          }else{
              let chosenImage = info[.originalImage] as! UIImage
              imgView.contentMode = .scaleAspectFill
              imgView.image = chosenImage
          }
        dismiss(animated: true, completion: nil)
      }
      
      
      func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
          dismiss(animated: true, completion: nil)
      }
}
