//
//  ConstantClass.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import Foundation
typealias alertCallBack = (_ type: String?) -> (Void)
public let titleError = "Please Enter Your Title"
public let descriptionError = "Please Enter Your Description"
public let appTitle = "To Do List"
public let cameraError = "Sorry, this device has no camera"
