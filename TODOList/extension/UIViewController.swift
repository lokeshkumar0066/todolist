//
//  UIViewController.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//


import Foundation
import UIKit

extension UIViewController{
        
    func showAlertWithCallback(message: String, title: String, buttonName: String, completionHandler: @escaping alertCallBack){
        let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction: UIAlertAction = UIAlertAction(title: buttonName, style: .cancel) { action -> Void in
            completionHandler(buttonName)
        }
        
        actionSheetController.addAction(okAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

