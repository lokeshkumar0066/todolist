//
//  UIView.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
        
    func dropShadow() {
      layer.masksToBounds = false
      layer.shadowColor = UIColor.lightGray.cgColor
      layer.shadowOpacity = 0.5
      layer.shadowOffset = CGSize(width: -1, height: 2)
      layer.shadowRadius = 5
      layer.cornerRadius = 4.0
    }
    
    func cornerRadiusWithBorder() {
        layer.masksToBounds = true
        layer.cornerRadius = 4.0
        layer.borderColor = UIColor.darkGray.cgColor
        layer.borderWidth = 1.0
    }
    
}
