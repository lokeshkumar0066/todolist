//
//  Date.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
