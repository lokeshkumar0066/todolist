//
//  UIImage.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
        
    func cornerRadius() {
        layer.masksToBounds = true
        layer.cornerRadius = self.frame.size.width / 2
        layer.borderColor = UIColor.darkGray.cgColor
        layer.borderWidth = 1.0
    }
    
}
