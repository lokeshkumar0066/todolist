//
//  UIButton.swift
//  TODOList
//
//  Created by manish on 23/06/20.
//  Copyright © 2020 Lokesh. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    
    func createCircle() {
        layer.masksToBounds = false
        layer.cornerRadius = self.frame.size.width / 2
    }
    
    
    func dropCornerRadius() {
        layer.masksToBounds = false
        layer.cornerRadius = 4
    }
}

